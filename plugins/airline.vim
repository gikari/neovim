" airline symbols
let g:airline_powerline_fonts = 1

" Buffers list
let g:airline#extensions#tabline#enabled = 1

