" Git Plugin configuration and maps

nnoremap <A-9> :G<CR>

nnoremap <C-K> :Gcommit<CR>

nnoremap <leader><C-K> :G push 

command! Gst G
command! -nargs=* Gco GCheckout <args>
command! -nargs=* Gc G commit <args>
command! -nargs=* Gd G diff <args>
command! -nargs=* Gdm G diff master <args>

