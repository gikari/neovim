" Leader
let mapleader = " "

" Buffer navigation
nnoremap <silent> <Tab> :bn<CR>
nnoremap <silent> <S-Tab> :bp<CR>
nnoremap <silent> <C-W><C-W> :bd<CR>

" Uppercase without move
nnoremap ~ mp~`pmp

" Fast console.log
nnoremap cl aconsole.log();0f(ci(

" Use TAB as Emmet Key
imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")

" Search for selected (highlighted) text, forwards or backwards.
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>

" Turn off highlight by \ + ]
nnoremap <silent> <leader>] :nohl<CR>

" Update file
nnoremap <F5> :e%<CR>

" Easier split navigation
nnoremap <silent> <A-Left> <C-W><Left>
nnoremap <silent> <A-Right> <C-W><Right>
nnoremap <silent> <A-Up> <C-W><Up>
nnoremap <silent> <A-Down> <C-W><Down>

" New Tab
nnoremap <silent> <C-t> :tabedit<CR>

" Tab navigation
nnoremap <C-Right> gt
nnoremap <C-Left> gT

" Simplier copy/paste
"vnoremap <C-c> "+y
"map <C-v> "+p

" Ctrl + / to toggle comment
map <C-_> <leader>c<space>

" Clear trailing spaces
nnoremap <silent> <C-L> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>

" Autoformat entire file
nnoremap <A-S-i> gg=G

" Fast changing of content after assignment operator
nnoremap cii; 0f=lct;<Space>

" Fast buffer navigation
nnoremap <C-Tab> :bn!<CR>
nnoremap <C-S-Tab> :bn!<CR>

" Fast brackets and quotes changng on a line
nnoremap cii( 0f(ci(
nnoremap cii) 0f(ci(

nnoremap cii{ 0f{ci{
nnoremap cii} 0f{ci{

nnoremap cii[ 0f[ci[
nnoremap cii] 0f[ci[

nnoremap cii" 0f"ci"
nnoremap c2ii" 0f";;ci"
nnoremap cii' 0f'ci'
nnoremap c2ii' 0f';;ci'

nnoremap ciit 0f>cit

" Ctrl + S to save
noremap <C-S>          :w<CR>
inoremap <C-S>         <Esc>:w<CR>
vnoremap <C-S>         <Esc>:w<CR>

" Fix unintuitive shortcuts
nmap Y y$

" Fixe mixtyping errors
ca WQ wq
ca Wq wq
ca W w
ca Q q

" Remove trailing spaces on save
fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun

autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()

" Quick edit init.vim
command! VIRC e ~/.config/nvim/init.vim

" Autosave
"autocmd TextChanged,TextChangedI <buffer> silent write
"set autowrite
"set autowriteall

" Terminal easy exit
tnoremap <Esc> <C-\><C-n>

" Split terminal
nnoremap <A-0> :sp term://zsh<CR>A

