source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/colorscheme.vim
source $HOME/.config/nvim/general.vim
source $HOME/.config/nvim/maps.vim

source $HOME/.config/nvim/plugins/airline.vim
source $HOME/.config/nvim/plugins/grammarous.vim
source $HOME/.config/nvim/plugins/fzf.vim
source $HOME/.config/nvim/plugins/cmake4vim.vim
source $HOME/.config/nvim/plugins/coc.vim
source $HOME/.config/nvim/plugins/firenvim.vim
source $HOME/.config/nvim/plugins/fugitive.vim
source $HOME/.config/nvim/plugins/highlightedyank.vim
source $HOME/.config/nvim/plugins/indentline.vim
source $HOME/.config/nvim/plugins/nerdtree.vim
source $HOME/.config/nvim/plugins/startify.vim
source $HOME/.config/nvim/plugins/xkbswitch.vim

