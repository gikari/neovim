" Plugin
call plug#begin('~/.local/share/nvim/plugged')

" Jupyter Notebook
Plug 'goerz/jupytext.vim'
"Plug 'szymonmaszke/vimpyter'

" Exchange text objects
Plug 'tommcdo/vim-exchange'

" Dev icons
Plug 'ryanoasis/vim-devicons'

" Grammar check
Plug 'rhysd/vim-grammarous'

" Debugger
Plug 'puremourning/vimspector'

" CMake Integration
Plug 'ilyachur/cmake4vim'

" Start Screen
Plug 'mhinz/vim-startify'

" C++ Semantic HighLight
Plug 'jackguo380/vim-lsp-cxx-highlight'

" NerdTree
Plug 'scrooloose/nerdtree'

" XEN Mode
"Plug 'junegunn/goyo.vim'

" XEN Mode addition
"Plug 'junegunn/limelight.vim'

" NVim in browser
"Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }

" Ranger file manager
"Plug 'francoiscabrol/ranger.vim'

" Dependency for the above
"Plug 'rbgrouleff/bclose.vim'

" Change the root directory to the file's one
Plug 'airblade/vim-rooter'

" Rust language support
Plug 'rust-lang/rust.vim'

" Nord color scheme
Plug 'arcticicestudio/nord-vim'

" Gruvbox theme
Plug 'morhetz/gruvbox'

" Multuple cursors sugar
Plug 'terryma/vim-multiple-cursors'

" Solarized colorscheme
Plug 'altercation/vim-colors-solarized'

" Repeat custom mappings
Plug 'tpope/vim-repeat'

" Highlight yanked text
Plug 'machakann/vim-highlightedyank'

" Switch keyboard layout when exiting insert mode
Plug 'lyokha/vim-xkbswitch'
"
" GLSL Support
Plug 'tikhomirov/vim-glsl'
" Support for a lot of languages
"Plug 'sheerun/vim-polyglot'
" Intellisense completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Color preview in CSS files
Plug 'ap/vim-css-color'
" Mark indentation with a | character
Plug 'yggdroot/indentline'
" C++ Enchanced support
Plug 'octol/vim-cpp-enhanced-highlight'
" Monokai colorscheme
Plug 'ErichDonGubler/vim-sublime-monokai'
" Pretty status line at the bottom off the editor
Plug 'vim-airline/vim-airline'
" That line themes
Plug 'vim-airline/vim-airline-themes'
" Marks on a linenumber bar
Plug 'kshenoy/vim-signature'
" Emmet - nuff said
Plug 'mattn/emmet-vim'

" Git commands in Command mode
Plug 'tpope/vim-fugitive'
Plug 'shumphrey/fugitive-gitlab.vim'
Plug 'tpope/vim-rhubarb'
" Git inlines
Plug 'airblade/vim-gitgutter'
" Support for . with plugins (e.g. vim-surround)
Plug 'tpope/vim-repeat'
" Surround text with braces\quotes
Plug 'tpope/vim-surround'

" Fuzzy search
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Branches with FZF
Plug 'stsewd/fzf-checkout.vim'

" Qml support
Plug 'peterhoeg/vim-qml'
" Quotes autocompletion
Plug 'raimondi/delimitmate'

" Fast commenting code
Plug 'scrooloose/nerdcommenter'
" Advanced syntax highlighting and linting
"Plug 'scrooloose/syntastic'

" Vue components highlight
Plug 'leafOfTree/vim-vue-plugin'

call plug#end()

