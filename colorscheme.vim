set termguicolors

" Monokai colorscheme and its fixes
" colorscheme sublimemonokai
" highlight HighlightedyankRegion cterm=reverse gui=reverse

" Nord colorscheme
colorscheme nord

" Gruvbox colorscheme
"colorscheme gruvbox

" Solarized colorscheme
" set background=light
" colorscheme solarized
