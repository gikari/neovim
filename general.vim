" Mouse support
set mouse=a

" Relative line numbers
set number relativenumber

" ???
set cursorline

" New Split appears on the right
set splitright
" New Split appears on the bottom
set splitbelow

" Case insensetive search
set ignorecase
set smartcase

syntax enable
set expandtab
" set shiftwidth=2
" set softtabstop=2
set shiftwidth=4
set softtabstop=4
set nohlsearch

" Set "+ as main buffer
set clipboard+=unnamedplus

" Set folding accordind to syntax
set foldmethod=syntax

" Unfold all folds on file open except comment header
set foldlevel=99
"set nofoldenable
"autocmd BufRead * silent! normal zczRzc

" Vue file preparations
autocmd FileType vue setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2

" Spell-check Git messages and Arcanist revisions
autocmd FileType gitcommit setlocal spell
autocmd FileType text setlocal spell

" Set syntax for alias file
autocmd BufReadPost aliasesrc set syntax=zsh

" Cursor forms
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

